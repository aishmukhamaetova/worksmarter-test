# README

## Applications

- @worksmarter/admin
- @worksmarter/ws
- @worksmarter/wsff

## Packages

- @worksmarter/admin--domains - Admin frontend state domains with effector
- @worksmarter/ws--domains - WS frontend state domains with effector
- @worksmarter/wsff--domains - WSFF frontend state domains with effector

- @worksmarter/admin--screens - Admin frontend screens
- @worksmarter/ws--screens - WS frontend screens
- @worksmarter/wsff--screens - WSFF frontend screens

## Frontend applications structure

```
app
  Route
    Page
      Screen
        Section
          Block
            Block
            Component
```

- Page - router connector to screen.
- Screen - page without knowing anything about router.
- Section - usual vertical part of page with domain connection.
- Block - logical block of section with domain connection.
- Component - any reusable pure component.

## Development

### Install dependencies

```
git clone xxx
cd worksmarter-test
npx lerna bootstrap
```

### RUN watcher

```
npx preconstruct watch
```

### Run frontend test application

```
yarn --cwd app/frontend start
```

### RUN admin application

_Watcher required._

```
yarn --cwd app/admin start
```

### RUN ws application

_Watcher required._

```
yarn --cwd app/ws start
```

### RUN wsff application

_Watcher required._

```
yarn --cwd app/wsff start
```
