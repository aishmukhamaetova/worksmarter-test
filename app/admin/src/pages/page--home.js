import React from 'react'
import {ScreenHome} from '@worksmarter/admin--screens'

export function PageHome() {
  return <ScreenHome />
}
