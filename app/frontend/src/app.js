import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {Switch} from 'react-router-dom'
import {Route} from 'react-router-dom'
import {PageHome} from './pages/page--home'
import {PageSign} from './pages/page--sign'
import {PagePdf} from './pages/page--pdf'

export function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <PageHome />
        </Route>
        <Route exact path="/sign">
          <PageSign />
        </Route>
        <Route exact path="/pdf">
          <PagePdf />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
