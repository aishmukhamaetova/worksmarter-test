import React from 'react'
import {BrowserRouter} from 'react-router-dom'
import {Switch} from 'react-router-dom'
import {Route} from 'react-router-dom'
import {PageHome} from './pages/page--home'

export function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <PageHome />
        </Route>
      </Switch>
    </BrowserRouter>
  )
}
