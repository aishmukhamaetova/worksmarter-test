import React from 'react'
import {ScreenHome} from '@worksmarter/ws--screens'

export function PageHome() {
  return <ScreenHome />
}
