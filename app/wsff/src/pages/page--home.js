import React from 'react'
import {ScreenHome} from '@worksmarter/wsff--screens'

export function PageHome() {
  return <ScreenHome />
}
