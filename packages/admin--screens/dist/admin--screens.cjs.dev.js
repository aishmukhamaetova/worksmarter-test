'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var styled = _interopDefault(require('styled-components'));
var ui = require('@worksmarter/ui');

function _templateObject2() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }
var A = {};
function ScreenHome() {
  return /*#__PURE__*/React.createElement(A.Screen, null, /*#__PURE__*/React.createElement(A.Title, null, "AdminScreens - ScreenHome"), /*#__PURE__*/React.createElement(ui.Button, null, "Admin button"));
}
A.Screen = styled.div(_templateObject());
A.Title = styled.div(_templateObject2());

exports.ScreenHome = ScreenHome;
