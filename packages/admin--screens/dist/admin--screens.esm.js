import React from 'react';
import styled from 'styled-components';
import { Button } from '@worksmarter/ui';

function _templateObject2() {
  var data = _taggedTemplateLiteral([""]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: grid;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }
var A = {};
function ScreenHome() {
  return /*#__PURE__*/React.createElement(A.Screen, null, /*#__PURE__*/React.createElement(A.Title, null, "AdminScreens - ScreenHome"), /*#__PURE__*/React.createElement(Button, null, "Admin button"));
}
A.Screen = styled.div(_templateObject());
A.Title = styled.div(_templateObject2());

export { ScreenHome };
