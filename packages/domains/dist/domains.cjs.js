'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./domains.cjs.prod.js");
} else {
  module.exports = require("./domains.cjs.dev.js");
}
