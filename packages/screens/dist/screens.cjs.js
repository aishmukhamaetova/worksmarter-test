'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./screens.cjs.prod.js");
} else {
  module.exports = require("./screens.cjs.dev.js");
}
