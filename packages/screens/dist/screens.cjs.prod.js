"use strict";

function _interopDefault(ex) {
  return ex && "object" == typeof ex && "default" in ex ? ex.default : ex;
}

Object.defineProperty(exports, "__esModule", {
  value: !0
});

var React = _interopDefault(require("react")), FormBuilder = _interopDefault(require("react-form-builder2"));

require("react-form-builder2/dist/app.css");

var styled = _interopDefault(require("styled-components")), effectorReact = require("effector-react"), entry_webpack = require("react-pdf/dist/entry.webpack"), effector = require("effector"), uuid = _interopDefault(require("uuid/v4")), reactRnd = require("react-rnd"), SignatureCanvas = _interopDefault(require("react-signature-canvas"));

function ScreenFormBuilderTest1() {
  return React.createElement(FormBuilder.ReactFormBuilder, null);
}

function _typeof(obj) {
  return (_typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(obj) {
    return typeof obj;
  } : function(obj) {
    return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  })(obj);
}

function _objectWithoutProperties(source, excluded) {
  if (null == source) return {};
  var key, i, target = _objectWithoutPropertiesLoose(source, excluded);
  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);
    for (i = 0; i < sourceSymbolKeys.length; i++) key = sourceSymbolKeys[i], excluded.indexOf(key) >= 0 || Object.prototype.propertyIsEnumerable.call(source, key) && (target[key] = source[key]);
  }
  return target;
}

function _objectWithoutPropertiesLoose(source, excluded) {
  if (null == source) return {};
  var key, i, target = {}, sourceKeys = Object.keys(source);
  for (i = 0; i < sourceKeys.length; i++) key = sourceKeys[i], excluded.indexOf(key) >= 0 || (target[key] = source[key]);
  return target;
}

function _toPropertyKey(arg) {
  var key = _toPrimitive(arg, "string");
  return "symbol" === _typeof(key) ? key : String(key);
}

function _toPrimitive(input, hint) {
  if ("object" !== _typeof(input) || null === input) return input;
  var prim = input[Symbol.toPrimitive];
  if (void 0 !== prim) {
    var res = prim.call(input, hint || "default");
    if ("object" !== _typeof(res)) return res;
    throw new TypeError("@@toPrimitive must return a primitive value.");
  }
  return ("string" === hint ? String : Number)(input);
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _unsupportedIterableToArray(o, minLen) {
  if (o) {
    if ("string" == typeof o) return _arrayLikeToArray(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    return "Object" === n && o.constructor && (n = o.constructor.name), "Map" === n || "Set" === n ? Array.from(o) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? _arrayLikeToArray(o, minLen) : void 0;
  }
}

function _iterableToArray(iter) {
  if ("undefined" != typeof Symbol && Symbol.iterator in Object(iter)) return Array.from(iter);
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _arrayLikeToArray(arr, len) {
  (null == len || len > arr.length) && (len = arr.length);
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);
  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);
    enumerableOnly && (symbols = symbols.filter((function(sym) {
      return Object.getOwnPropertyDescriptor(object, sym).enumerable;
    }))), keys.push.apply(keys, symbols);
  }
  return keys;
}

function _objectSpread(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = null != arguments[i] ? arguments[i] : {};
    i % 2 ? ownKeys(Object(source), !0).forEach((function(key) {
      _defineProperty(target, key, source[key]);
    })) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach((function(key) {
      Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
    }));
  }
  return target;
}

function _defineProperty(obj, key, value) {
  return key in obj ? Object.defineProperty(obj, key, {
    value: value,
    enumerable: !0,
    configurable: !0,
    writable: !0
  }) : obj[key] = value, obj;
}

function makeOne() {
  return {
    id: uuid(),
    name: "Input",
    x: 0,
    y: 0,
    width: 120,
    height: 20
  };
}

var who = [ "fields" ], domain = effector.createDomain(who.join(".")), debug = function() {
  for (var _console, _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) args[_key] = arguments[_key];
  return (_console = console).log.apply(_console, who.concat(args));
}, $ = {}, on = {}, run = {}, initial = [ makeOne(), makeOne() ];

$.data = domain.store(initial.reduce((function(r, v) {
  return r[v.id] = v, r;
}), {}), {
  name: "$.data"
}), $.order = domain.store(initial.map((function(_ref) {
  return _ref.id;
})), {
  name: "$.order"
}), on.upsert = effector.createEvent("on.upsert"), on.create = effector.createEvent("on.create"), 
on.update = effector.createEvent("on.update"), on.remove = effector.createEvent("on.remove"), 
run.upsert = effector.createEffect({
  name: "run.upsert",
  handler: function(_ref2) {
    var data = _ref2.data, doc = _ref2.doc;
    return doc.id in data ? on.update(doc) : on.create(doc);
  }
}), effector.sample({
  source: $.data,
  clock: on.upsert,
  fn: function(data, doc) {
    return {
      data: data,
      doc: doc
    };
  },
  target: run.upsert
}), $.data.on(on.create, (function(state, doc) {
  return _objectSpread(_objectSpread({}, state), {}, _defineProperty({}, doc.id, doc));
})), $.order.on(on.create, (function(state, doc) {
  return [].concat(_toConsumableArray(state), [ doc.id ]);
})), $.data.on(on.update, (function(state, doc) {
  return _objectSpread(_objectSpread({}, state), {}, _defineProperty({}, doc.id, _objectSpread(_objectSpread({}, state[doc.id]), doc)));
})), $.data.on(on.remove, (function(state, doc) {
  var _doc$id = doc.id;
  state[_doc$id];
  return _objectWithoutProperties(state, [ _doc$id ].map(_toPropertyKey));
})), $.order.on(on.remove, (function(state, doc) {
  return state.filter((function(v) {
    return v !== doc.id;
  }));
})), $.data.watch((function(_) {
  return debug("$.data", _);
})), $.order.watch((function(_) {
  return debug("$.order", _);
}));

var fromFields = {
  $: $,
  on: on,
  run: run,
  util: {
    makeOne: makeOne
  }
}, who$1 = [ "fields" ], domain$1 = effector.createDomain(who$1.join(".")), debug$1 = function() {
  for (var _console, _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) args[_key] = arguments[_key];
  return (_console = console).log.apply(_console, who$1.concat(args));
}, $$1 = {}, on$1 = {}, run$1 = {};

on$1.toggle = domain$1.event(), $$1.isPreview = domain$1.store(!1), $$1.isPreview.on(on$1.toggle, (function(v) {
  return !v;
})), on$1.file = domain$1.event(), $$1.file = effector.restore(on$1.file, null), 
$$1.file.watch((function(v) {
  return debug$1("$.file", v);
})), run$1.add = domain$1.effect({
  handler: function() {
    fromFields.on.upsert(fromFields.util.makeOne());
  }
});

var fromEditor = {
  $: $$1,
  on: on$1,
  run: run$1
};

function _slicedToArray(arr, i) {
  return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$1(arr, i) || _nonIterableRest();
}

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _unsupportedIterableToArray$1(o, minLen) {
  if (o) {
    if ("string" == typeof o) return _arrayLikeToArray$1(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    return "Object" === n && o.constructor && (n = o.constructor.name), "Map" === n || "Set" === n ? Array.from(o) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? _arrayLikeToArray$1(o, minLen) : void 0;
  }
}

function _arrayLikeToArray$1(arr, len) {
  (null == len || len > arr.length) && (len = arr.length);
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}

function _iterableToArrayLimit(arr, i) {
  if ("undefined" != typeof Symbol && Symbol.iterator in Object(arr)) {
    var _arr = [], _n = !0, _d = !1, _e = void 0;
    try {
      for (var _s, _i = arr[Symbol.iterator](); !(_n = (_s = _i.next()).done) && (_arr.push(_s.value), 
      !i || _arr.length !== i); _n = !0) ;
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        _n || null == _i.return || _i.return();
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

var defaults = {
  file: "/test.pdf"
};

function SectionDocument() {
  var file = effectorReact.useStore(fromEditor.$.file), pageNumber = _slicedToArray(React.useState(1), 1)[0];
  return React.createElement(entry_webpack.Document, {
    file: file || defaults.file,
    onLoadSuccess: function() {}
  }, React.createElement(entry_webpack.Page, {
    pageNumber: pageNumber,
    scale: 1
  }));
}

function _templateObject() {
  var data = _taggedTemplateLiteral([ "\n  display: flex !important;\n  align-items: center;\n  justify-content: center;\n  ", "\n\n  & input {\n    width: 100%;\n    height: 100%;\n    border: none;\n  }\n" ]);
  return _templateObject = function() {
    return data;
  }, data;
}

function _taggedTemplateLiteral(strings, raw) {
  return raw || (raw = strings.slice(0)), Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

function _slicedToArray$1(arr, i) {
  return _arrayWithHoles$1(arr) || _iterableToArrayLimit$1(arr, i) || _unsupportedIterableToArray$2(arr, i) || _nonIterableRest$1();
}

function _nonIterableRest$1() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function _unsupportedIterableToArray$2(o, minLen) {
  if (o) {
    if ("string" == typeof o) return _arrayLikeToArray$2(o, minLen);
    var n = Object.prototype.toString.call(o).slice(8, -1);
    return "Object" === n && o.constructor && (n = o.constructor.name), "Map" === n || "Set" === n ? Array.from(o) : "Arguments" === n || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n) ? _arrayLikeToArray$2(o, minLen) : void 0;
  }
}

function _arrayLikeToArray$2(arr, len) {
  (null == len || len > arr.length) && (len = arr.length);
  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];
  return arr2;
}

function _iterableToArrayLimit$1(arr, i) {
  if ("undefined" != typeof Symbol && Symbol.iterator in Object(arr)) {
    var _arr = [], _n = !0, _d = !1, _e = void 0;
    try {
      for (var _s, _i = arr[Symbol.iterator](); !(_n = (_s = _i.next()).done) && (_arr.push(_s.value), 
      !i || _arr.length !== i); _n = !0) ;
    } catch (err) {
      _d = !0, _e = err;
    } finally {
      try {
        _n || null == _i.return || _i.return();
      } finally {
        if (_d) throw _e;
      }
    }
    return _arr;
  }
}

function _arrayWithHoles$1(arr) {
  if (Array.isArray(arr)) return arr;
}

var A = {};

function Field(_ref) {
  var onMove = _ref.onMove, onResize = _ref.onResize, isPreview = _ref.isPreview, _React$useState2 = _slicedToArray$1(React.useState(""), 2), value = _React$useState2[0], setValue = _React$useState2[1];
  var enableResizing = isPreview ? {} : {
    bottomRight: !0
  };
  return React.createElement(A.Card, {
    disableDragging: isPreview,
    enableResizing: enableResizing,
    default: {
      x: 0,
      y: 0,
      width: 320,
      height: 200
    },
    onDragStop: onMove,
    onResizeStop: onResize,
    isPreview: isPreview
  }, isPreview ? React.createElement("input", {
    value: value,
    onChange: function(_ref2) {
      var value = _ref2.target.value;
      setValue(value);
    }
  }) : "Move and resize me");
}

var on$2 = {};

function SectionFieldsEditor() {
  var order = effectorReact.useStore(fromFields.$.order), isPreview = effectorReact.useStore(fromEditor.$.isPreview);
  return order.map((function(id) {
    return React.createElement(Field, {
      key: id,
      onMove: function(e, d) {
        return on$2.move({
          id: id,
          e: e,
          d: d
        });
      },
      onResize: function(e, direction, ref) {
        return on$2.resize({
          id: id,
          e: e,
          direction: direction,
          ref: ref
        });
      },
      isPreview: isPreview
    });
  }));
}

on$2.move = fromFields.on.update.prepend((function(_ref3) {
  var id = _ref3.id, d = (_ref3.e, _ref3.d);
  return {
    id: id,
    x: d.x,
    y: d.y
  };
})), on$2.resize = fromFields.on.update.prepend((function(_ref4) {
  var id = _ref4.id, ref = (_ref4.e, _ref4.direction, _ref4.ref);
  return {
    id: id,
    width: ref.style.width,
    height: ref.style.height
  };
}));

var cardStyle = function(_ref5) {
  return _ref5.isPreview ? "" : "\n  border: solid 1px #ddd;\n  background: #fefef0;\n";
};

A.Card = styled(reactRnd.Rnd)(_templateObject(), cardStyle);

var t = {
  add: "Add field",
  preview: "Switch to editor",
  editor: "Switch to preview"
};

function SectionToolbar() {
  var isPreview = effectorReact.useStore(fromEditor.$.isPreview);
  return React.createElement(React.Fragment, null, React.createElement("button", {
    onClick: fromEditor.run.add
  }, t.add), React.createElement("button", {
    onClick: fromEditor.on.toggle
  }, isPreview ? t.preview : t.editor));
}

var on$3 = {};

function SectionUpload() {
  return React.createElement("input", {
    type: "file",
    onChange: on$3.change
  });
}

function _templateObject$1() {
  var data = _taggedTemplateLiteral$1([ "\n  display: grid;\n" ]);
  return _templateObject$1 = function() {
    return data;
  }, data;
}

function _taggedTemplateLiteral$1(strings, raw) {
  return raw || (raw = strings.slice(0)), Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

on$3.change = fromEditor.on.file.prepend((function(e) {
  return e.target.files[0];
}));

var A$1 = {};

function ScreenPdfFormBuilderTest1() {
  return React.createElement(A$1.Screen, null, React.createElement(SectionDocument, null), React.createElement(SectionFieldsEditor, null), React.createElement(SectionToolbar, null), React.createElement(SectionUpload, null));
}

function ScreenSignatureTest1() {
  return React.createElement(SignatureCanvas, {
    penColor: "black",
    canvasProps: {
      width: 500,
      height: 200,
      className: "sigCanvas"
    }
  });
}

A$1.Screen = styled.div(_templateObject$1()), exports.ScreenFormBuilderTest1 = ScreenFormBuilderTest1, 
exports.ScreenPdfFormBuilderTest1 = ScreenPdfFormBuilderTest1, exports.ScreenSignatureTest1 = ScreenSignatureTest1;
