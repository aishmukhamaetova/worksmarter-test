import React from 'react';
import FormBuilder from 'react-form-builder2';
import 'react-form-builder2/dist/app.css';
import styled from 'styled-components';
import { useStore } from 'effector-react';
import { Document, Page } from 'react-pdf/dist/entry.webpack';
import { createDomain, createEvent, createEffect, sample, restore } from 'effector';
import uuid from 'uuid/v4';
import { Rnd } from 'react-rnd';
import SignatureCanvas from 'react-signature-canvas';

function ScreenFormBuilderTest1() {
  return /*#__PURE__*/React.createElement(FormBuilder.ReactFormBuilder, null);
}

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }

function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function makeOne() {
  return {
    id: uuid(),
    name: "Input",
    x: 0,
    y: 0,
    width: 120,
    height: 20
  };
}

var who = ["fields"];
var domain = createDomain(who.join("."));

var debug = function debug() {
  var _console;

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return (_console = console).log.apply(_console, who.concat(args));
};

var $ = {};
var on = {};
var run = {};
var initial = [makeOne(), makeOne()];
$.data = domain.store(initial.reduce(function (r, v) {
  return r[v.id] = v, r;
}, {}), {
  name: "$.data"
});
$.order = domain.store(initial.map(function (_ref) {
  var id = _ref.id;
  return id;
}), {
  name: "$.order"
});
on.upsert = createEvent("on.upsert");
on.create = createEvent("on.create");
on.update = createEvent("on.update");
on.remove = createEvent("on.remove");
run.upsert = createEffect({
  name: "run.upsert",
  handler: function handler(_ref2) {
    var data = _ref2.data,
        doc = _ref2.doc;
    return doc.id in data ? on.update(doc) : on.create(doc);
  }
});
sample({
  source: $.data,
  clock: on.upsert,
  fn: function fn(data, doc) {
    return {
      data: data,
      doc: doc
    };
  },
  target: run.upsert
});
$.data.on(on.create, function (state, doc) {
  return _objectSpread(_objectSpread({}, state), {}, _defineProperty({}, doc.id, doc));
});
$.order.on(on.create, function (state, doc) {
  return [].concat(_toConsumableArray(state), [doc.id]);
});
$.data.on(on.update, function (state, doc) {
  return _objectSpread(_objectSpread({}, state), {}, _defineProperty({}, doc.id, _objectSpread(_objectSpread({}, state[doc.id]), doc)));
});
$.data.on(on.remove, function (state, doc) {
  var _doc$id = doc.id,
      r = state[_doc$id],
      rest = _objectWithoutProperties(state, [_doc$id].map(_toPropertyKey));

  return rest;
});
$.order.on(on.remove, function (state, doc) {
  return state.filter(function (v) {
    return v !== doc.id;
  });
});
$.data.watch(function (_) {
  return debug("$.data", _);
});
$.order.watch(function (_) {
  return debug("$.order", _);
}); // $.fields = createStore([makeOne(), makeOne()])
// $.fields.on(on.create, state => [...state, makeOne()])
// $.fields.on(on.update, state => [])

var fromFields = {
  $: $,
  on: on,
  run: run,
  util: {
    makeOne: makeOne
  }
};

var who$1 = ["fields"];
var domain$1 = createDomain(who$1.join("."));

var debug$1 = function debug() {
  var _console;

  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return (_console = console).log.apply(_console, who$1.concat(args));
};

var $$1 = {};
var on$1 = {};
var run$1 = {};
on$1.toggle = domain$1.event();
$$1.isPreview = domain$1.store(false);
$$1.isPreview.on(on$1.toggle, function (v) {
  return !v;
});
on$1.file = domain$1.event();
$$1.file = restore(on$1.file, null);
$$1.file.watch(function (v) {
  return debug$1("$.file", v);
});
run$1.add = domain$1.effect({
  handler: function handler() {
    fromFields.on.upsert(fromFields.util.makeOne());
  }
});
var fromEditor = {
  $: $$1,
  on: on$1,
  run: run$1
};

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray$1(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray$1(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray$1(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$1(o, minLen); }

function _arrayLikeToArray$1(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var defaults = {
  file: "/test.pdf"
};
function SectionDocument() {
  var file = useStore(fromEditor.$.file);

  var _React$useState = React.useState(1),
      _React$useState2 = _slicedToArray(_React$useState, 1),
      pageNumber = _React$useState2[0];

  function onDocumentLoadSuccess() {}

  return /*#__PURE__*/React.createElement(Document, {
    file: file ? file : defaults.file,
    onLoadSuccess: onDocumentLoadSuccess
  }, /*#__PURE__*/React.createElement(Page, {
    pageNumber: pageNumber,
    scale: 1
  }));
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n  display: flex !important;\n  align-items: center;\n  justify-content: center;\n  ", "\n\n  & input {\n    width: 100%;\n    height: 100%;\n    border: none;\n  }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _slicedToArray$1(arr, i) { return _arrayWithHoles$1(arr) || _iterableToArrayLimit$1(arr, i) || _unsupportedIterableToArray$2(arr, i) || _nonIterableRest$1(); }

function _nonIterableRest$1() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray$2(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray$2(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray$2(o, minLen); }

function _arrayLikeToArray$2(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit$1(arr, i) { if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return; var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles$1(arr) { if (Array.isArray(arr)) return arr; }
var A = {};

function Field(_ref) {
  var onMove = _ref.onMove,
      onResize = _ref.onResize,
      isPreview = _ref.isPreview;

  var _React$useState = React.useState(""),
      _React$useState2 = _slicedToArray$1(_React$useState, 2),
      value = _React$useState2[0],
      setValue = _React$useState2[1];

  function onChange(_ref2) {
    var value = _ref2.target.value;
    setValue(value);
  }

  var enableResizing = isPreview ? {} : {
    bottomRight: true
  };
  return /*#__PURE__*/React.createElement(A.Card, {
    disableDragging: isPreview,
    enableResizing: enableResizing,
    "default": {
      x: 0,
      y: 0,
      width: 320,
      height: 200
    },
    onDragStop: onMove,
    onResizeStop: onResize,
    isPreview: isPreview
  }, isPreview ? /*#__PURE__*/React.createElement("input", {
    value: value,
    onChange: onChange
  }) : "Move and resize me");
}

var on$2 = {};
on$2.move = fromFields.on.update.prepend(function (_ref3) {
  var id = _ref3.id,
      e = _ref3.e,
      d = _ref3.d;
  return {
    id: id,
    x: d.x,
    y: d.y
  };
});
on$2.resize = fromFields.on.update.prepend(function (_ref4) {
  var id = _ref4.id,
      e = _ref4.e,
      direction = _ref4.direction,
      ref = _ref4.ref;
  return {
    id: id,
    width: ref.style.width,
    height: ref.style.height
  };
});
function SectionFieldsEditor() {
  var order = useStore(fromFields.$.order);
  var isPreview = useStore(fromEditor.$.isPreview);
  return order.map(function (id) {
    return /*#__PURE__*/React.createElement(Field, {
      key: id,
      onMove: function onMove(e, d) {
        return on$2.move({
          id: id,
          e: e,
          d: d
        });
      },
      onResize: function onResize(e, direction, ref) {
        return on$2.resize({
          id: id,
          e: e,
          direction: direction,
          ref: ref
        });
      },
      isPreview: isPreview
    });
  });
}

var cardStyle = function cardStyle(_ref5) {
  var isPreview = _ref5.isPreview;
  return isPreview ? "" : "\n  border: solid 1px #ddd;\n  background: #fefef0;\n";
};

A.Card = styled(Rnd)(_templateObject(), cardStyle);

var t = {
  add: "Add field",
  preview: "Switch to editor",
  editor: "Switch to preview"
};
function SectionToolbar() {
  var isPreview = useStore(fromEditor.$.isPreview);
  return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("button", {
    onClick: fromEditor.run.add
  }, t.add), /*#__PURE__*/React.createElement("button", {
    onClick: fromEditor.on.toggle
  }, isPreview ? t.preview : t.editor));
}

var on$3 = {};
on$3.change = fromEditor.on.file.prepend(function (e) {
  return e.target.files[0];
});
function SectionUpload() {
  return /*#__PURE__*/React.createElement("input", {
    type: "file",
    onChange: on$3.change
  });
}

function _templateObject$1() {
  var data = _taggedTemplateLiteral$1(["\n  display: grid;\n"]);

  _templateObject$1 = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral$1(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }
var A$1 = {};
function ScreenPdfFormBuilderTest1() {
  return /*#__PURE__*/React.createElement(A$1.Screen, null, /*#__PURE__*/React.createElement(SectionDocument, null), /*#__PURE__*/React.createElement(SectionFieldsEditor, null), /*#__PURE__*/React.createElement(SectionToolbar, null), /*#__PURE__*/React.createElement(SectionUpload, null));
}
A$1.Screen = styled.div(_templateObject$1());

function ScreenSignatureTest1() {
  return /*#__PURE__*/React.createElement(SignatureCanvas, {
    penColor: "black",
    canvasProps: {
      width: 500,
      height: 200,
      className: 'sigCanvas'
    }
  });
}

export { ScreenFormBuilderTest1, ScreenPdfFormBuilderTest1, ScreenSignatureTest1 };
