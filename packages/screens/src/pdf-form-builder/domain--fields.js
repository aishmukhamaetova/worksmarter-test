import {createDomain} from 'effector'
import {createEvent} from 'effector'
import {createEffect} from 'effector'
import {sample} from 'effector'
import uuid from 'uuid/v4'

function makeOne() {
  return {
    id: uuid(),
    name: `Input`,
    x: 0,
    y: 0,
    width: 120,
    height: 20,
  }
}

const who = [`fields`]

const domain = createDomain(who.join(`.`))

const debug = (...args) => console.log(...who, ...args)

const $ = {}
const on = {}
const run = {}

const initial = [makeOne(), makeOne()]

$.data = domain.store(
  initial.reduce((r, v) => ((r[v.id] = v), r), {}),
  {name: `$.data`},
)
$.order = domain.store(
  initial.map(({id}) => id),
  {name: `$.order`},
)

on.upsert = createEvent(`on.upsert`)
on.create = createEvent(`on.create`)
on.update = createEvent(`on.update`)
on.remove = createEvent(`on.remove`)

run.upsert = createEffect({
  name: `run.upsert`,
  handler({data, doc}) {
    return doc.id in data ? on.update(doc) : on.create(doc)
  },
})

sample({
  source: $.data,
  clock: on.upsert,
  fn: (data, doc) => ({data, doc}),
  target: run.upsert,
})

$.data.on(on.create, (state, doc) => ({...state, [doc.id]: doc}))
$.order.on(on.create, (state, doc) => [...state, doc.id])

$.data.on(on.update, (state, doc) => ({
  ...state,
  [doc.id]: {...state[doc.id], ...doc},
}))

$.data.on(on.remove, (state, doc) => {
  const {[doc.id]: r, ...rest} = state
  return rest
})
$.order.on(on.remove, (state, doc) => state.filter((v) => v !== doc.id))

$.data.watch((_) => debug(`$.data`, _))
$.order.watch((_) => debug(`$.order`, _))

// $.fields = createStore([makeOne(), makeOne()])

// $.fields.on(on.create, state => [...state, makeOne()])

// $.fields.on(on.update, state => [])

export const fromFields = {
  $,
  on,
  run,
  util: {
    makeOne,
  },
}
