import React from 'react'
import styled from 'styled-components'

import {SectionDocument} from './section--document'
import {SectionFieldsEditor} from './section--fields-editor'
import {SectionToolbar} from './section--toolbar'
import {SectionUpload} from './section--upload'

const A = {}

export function ScreenPdfFormBuilderTest1() {
  return (
    <A.Screen>
      <SectionDocument />
      <SectionFieldsEditor />
      <SectionToolbar />
      <SectionUpload />
    </A.Screen>
  )
}

A.Screen = styled.div`
  display: grid;
`
