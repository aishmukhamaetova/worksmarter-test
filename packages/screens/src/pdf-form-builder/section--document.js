import {useStore} from 'effector-react'
import React from 'react'
import {Document, Page} from 'react-pdf/dist/entry.webpack'

import {fromEditor} from './domain--editor'

const defaults = {
  file: `/test.pdf`,
}

export function SectionDocument() {
  const file = useStore(fromEditor.$.file)

  const [pageNumber] = React.useState(1)

  function onDocumentLoadSuccess() {}

  return (
    <Document
      file={file ? file : defaults.file}
      onLoadSuccess={onDocumentLoadSuccess}
    >
      <Page pageNumber={pageNumber} scale={1} />
    </Document>
  )
}
