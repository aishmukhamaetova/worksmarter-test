import React from 'react'
import {useStore} from 'effector-react'

import {fromEditor} from './domain--editor'

const t = {
  add: `Add field`,
  preview: `Switch to editor`,
  editor: `Switch to preview`,
}

export function SectionToolbar() {
  const isPreview = useStore(fromEditor.$.isPreview)

  return (
    <>
      <button onClick={fromEditor.run.add}>{t.add}</button>
      <button onClick={fromEditor.on.toggle}>
        {isPreview ? t.preview : t.editor}
      </button>
    </>
  )
}
