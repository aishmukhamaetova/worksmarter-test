import React from 'react'
import SignatureCanvas from 'react-signature-canvas'

export function ScreenSignatureTest1() {
  return (
    <SignatureCanvas
      penColor="black"
      canvasProps={{width: 500, height: 200, className: 'sigCanvas'}}
    />
  )
}
