'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var core = require('@material-ui/core');

function Button(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/React.createElement(core.Button, {
    color: "primary"
  }, children);
}

exports.Button = Button;
