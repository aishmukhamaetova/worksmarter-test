'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./ui.cjs.prod.js");
} else {
  module.exports = require("./ui.cjs.dev.js");
}
