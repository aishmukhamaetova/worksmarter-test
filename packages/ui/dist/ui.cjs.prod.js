"use strict";

function _interopDefault(ex) {
  return ex && "object" == typeof ex && "default" in ex ? ex.default : ex;
}

Object.defineProperty(exports, "__esModule", {
  value: !0
});

var React = _interopDefault(require("react")), core = require("@material-ui/core");

function Button(_ref) {
  var children = _ref.children;
  return React.createElement(core.Button, {
    color: "primary"
  }, children);
}

exports.Button = Button;
