import React from 'react';
import { Button as Button$1 } from '@material-ui/core';

function Button(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/React.createElement(Button$1, {
    color: "primary"
  }, children);
}

export { Button };
