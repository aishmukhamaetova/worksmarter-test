import React from 'react'
import {Button as B} from '@material-ui/core'

export function Button({children}) {
  return <B color="primary">{children}</B>
}
