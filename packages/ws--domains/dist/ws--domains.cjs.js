'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./ws--domains.cjs.prod.js");
} else {
  module.exports = require("./ws--domains.cjs.dev.js");
}
