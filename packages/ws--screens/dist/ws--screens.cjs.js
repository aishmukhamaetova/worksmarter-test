'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./ws--screens.cjs.prod.js");
} else {
  module.exports = require("./ws--screens.cjs.dev.js");
}
