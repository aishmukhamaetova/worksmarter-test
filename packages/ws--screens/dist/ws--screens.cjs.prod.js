"use strict";

function _interopDefault(ex) {
  return ex && "object" == typeof ex && "default" in ex ? ex.default : ex;
}

Object.defineProperty(exports, "__esModule", {
  value: !0
});

var React = _interopDefault(require("react")), styled = _interopDefault(require("styled-components")), ui = require("@worksmarter/ui");

function _templateObject2() {
  var data = _taggedTemplateLiteral([ "" ]);
  return _templateObject2 = function() {
    return data;
  }, data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral([ "\n  display: grid;\n" ]);
  return _templateObject = function() {
    return data;
  }, data;
}

function _taggedTemplateLiteral(strings, raw) {
  return raw || (raw = strings.slice(0)), Object.freeze(Object.defineProperties(strings, {
    raw: {
      value: Object.freeze(raw)
    }
  }));
}

var A = {};

function ScreenHome() {
  return React.createElement(A.Screen, null, React.createElement(A.Title, null, "WsScreens - ScreenHome"), React.createElement(ui.Button, null, "WS Button"));
}

A.Screen = styled.div(_templateObject()), A.Title = styled.div(_templateObject2()), 
exports.ScreenHome = ScreenHome;
