'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./wsff--domains.cjs.prod.js");
} else {
  module.exports = require("./wsff--domains.cjs.dev.js");
}
