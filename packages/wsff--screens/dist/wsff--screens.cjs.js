'use strict';

if (process.env.NODE_ENV === "production") {
  module.exports = require("./wsff--screens.cjs.prod.js");
} else {
  module.exports = require("./wsff--screens.cjs.dev.js");
}
