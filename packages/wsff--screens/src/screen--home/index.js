import React from 'react'
import styled from 'styled-components'

import {Button} from '@worksmarter/ui'

const A = {}

export function ScreenHome() {
  return (
    <A.Screen>
      <A.Title>WsffScreens - ScreenHome</A.Title>
      <Button>WSFF Button</Button>
    </A.Screen>
  )
}

A.Screen = styled.div`
  display: grid;
`

A.Title = styled.div``
